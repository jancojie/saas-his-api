<?php

namespace SaasHisApi;

class SaasHisConstant
{
    public static $METHOD_POST = "POST";
    public static $METHOD_GET = "GET";
    public static $RESPONSE_ARRAY = "Array";
    public static $RESPONSE_OBJECT = "Object";
    public static $RESPONSE_STRING = "String";
}