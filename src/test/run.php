<?php
require '../../vendor/autoload.php';

use SaasHisApi\Request\OpenApiAuthTokenGetRequest;
use SaasHisApi\Request\OpenApiUserListRequest;
use SaasHisApi\SaasHisClient;


$client = new SaasHisClient();

$tokenRequest = new OpenApiAuthTokenGetRequest;
$tokenRequest->setAppKey('');
$tokenRequest->setAppSecret('');

$data = $client->execute($tokenRequest);
if (isset($data['errorCode']) && $data['errorCode'] == 200) {
    $accessToken = $data['data'];

    $userListRequest = new OpenApiUserListRequest;
    $list = $client->execute($userListRequest, $accessToken);
    var_dump($list);
}
