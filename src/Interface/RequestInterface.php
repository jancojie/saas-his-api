<?php

namespace SaasHisApi\Interface;


interface RequestInterface{

    /**
     * 获取请求接口的信息
     *
     * @return String
     */
    public function getApiName():string;


    /**
     * 获取请求数组
     *
     * @return array
     */
    public function getParam():array;

    /**
     * 检查请求数据是否符合条件
     *
     * @return bool
     */
    public function check():bool;

    /**
     * 获取请求接口的请求类型
     *
     * @return string
     */
    public function getApiMethod():string;
}