<?php 

namespace SaasHisApi\Request;

use SaasHisApi\Interface\RequestInterface;
use SaasHisApi\SaasHisConstant;

class OpenApiUserListRequest implements RequestInterface{

    private $name;
    private $phone;
    private $dossierNumber;

    private $pageNum = 1;
    private $pageSize = 1000;

    public function getParam(): array
    {
        return array_filter(get_object_vars($this));
    }
    
    public function check(): bool
    {
        return true;
    }

    public function getApiName(): string
    {
        return 'openapi/users/list';
    }

    public function getApiMethod():string
    {
        return SaasHisConstant::$METHOD_POST;
    }

    public function setPageNum($pageNum)
    {
        $this->pageNum = $pageNum;
    }


    public function setPageSize($pageSize)
    {
        $this->pageSize = $pageSize;
    }


    public function setName($name){
        $this->name = $name;
    }

    public function setDossierNumber($dossierNumber)
    {
        $this->dossierNumber = $dossierNumber;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }


    public function getPageSize()
    {
        return $this->pageSize;
    }


    public function getPageNum()
    {
        return $this->pageNum;
    }

    public function getName(){
        return $this->name;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getDossierNumber()
    {
        return $this->dossierNumber;
    }
}