<?php 

namespace SaasHisApi\Request;

use SaasHisApi\Interface\RequestInterface;
use SaasHisApi\SaasHisConstant;

class OpenApiAuthTokenGetRequest implements RequestInterface{

    private $appKey;
    private $appSecret;

    public function getParam(): array
    {
        return array_filter(get_object_vars($this));
    }

    public function check(): bool
    {
        return true;
    }

    public function getApiName(): string
    {
        return 'openapi/auth/token/get';
    }

    public function getApiMethod():string
    {
        return SaasHisConstant::$METHOD_POST;
    }

    public function setAppKey($appKey){
        $this->appKey = $appKey;
    }

    public function setAppSecret($appSecret)
    {
        $this->appSecret = $appSecret;
    }

    public function getAppKey(){
        return $this->appKey;
    }

    public function getAppSecret()
    {
        return $this->appSecret;
    }
}