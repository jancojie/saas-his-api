<?php 

namespace SaasHisApi\Request;

use SaasHisApi\Interface\RequestInterface;
use SaasHisApi\SaasHisConstant;

class OpenApiSchedulingRequest implements RequestInterface{

    private $date;

    public function getParam(): array
    {
        return array_filter(get_object_vars($this));
    }

    public function check(): bool
    {
        return true;
    }

    public function getApiName(): string
    {
        return 'openapi/scheduling';
    }

    public function getApiMethod():string
    {
        return SaasHisConstant::$METHOD_POST;
    }

    public function setDate($date)
    {
        $this->date = $date;
    }

    public function getDate()
    {
        return $this->date;
    }

}