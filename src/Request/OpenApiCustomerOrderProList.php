<?php

namespace SaasHisApi\Request;

use SaasHisApi\Interface\RequestInterface;
use SaasHisApi\SaasHisConstant;

class OpenApiCustomerOrderProList implements RequestInterface
{

    // 1显示，0不显示。是否显示无效订单，无效订单包括废弃订单、撤掉订单
    private $invalidFlag;
    // 订单状态：1已结算，2已结清，3待还款，4已撤销，5已退回，6已弃单，-1已退款
    private $orderState;
    // 用户ID
    private $userId;
    // 下单开始时间
    private $startDate;
    // 下单结束时间
    private $endDate;
    // 订单类型：1项目/套餐订单，2售卡订单，3充值订单，4订金订单，5处方订单
    private $orderType;
    // 项目名称
    private $projectName;
    // 订单编号
    private $orderNumber;
    // 用户姓名
    private $userName;


    private $pageNum = 1;
    private $pageSize = 1000;

    public function getParam(): array
    {
        return array_filter(get_object_vars($this));
    }

    public function check(): bool
    {
        return true;
    }

    public function getApiName(): string
    {
        return 'openapi/customerOrderPro/list';
    }

    public function getApiMethod(): string
    {
        return SaasHisConstant::$METHOD_POST;
    }

    public function setInvalidFlag($invalidFlag)
    {
        $this->invalidFlag = $invalidFlag;
    }


    public function setOrderState($orderState)
    {
        $this->orderState = $orderState;
    }


    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    public function setOrderType($orderType)
    {
        $this->orderType = $orderType;
    }

    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;
    }
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    public function setUserName($userName)
    {
        $this->userName = $userName;
    }


    public function setPageNum($pageNum)
    {
        $this->pageNum = $pageNum;
    }


    public function setPageSize($pageSize)
    {
        $this->pageSize = $pageSize;
    }


    public function getInvalidFlag()
    {
        return  $this->invalidFlag;
    }


    public function getOrderState()
    {
        return $this->orderState;
    }


    public function getUserId()
    {
        return $this->userId;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    public function getOrderType()
    {
        return $this->orderType;
    }

    public function getProjectName()
    {
        return $this->projectName;
    }
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    public function getUserName()
    {
        return  $this->userName;
    }

    public function getPageSize()
    {
        return $this->pageSize;
    }


    public function getPageNum()
    {
        return $this->pageNum;
    }

}
