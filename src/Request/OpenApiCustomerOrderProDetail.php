<?php

namespace SaasHisApi\Request;

use SaasHisApi\Interface\RequestInterface;
use SaasHisApi\SaasHisConstant;

class OpenApiCustomerOrderProDetail implements RequestInterface
{

    private $orderCode;

    public function getParam(): array
    {
        return array_filter(get_object_vars($this));
    }

    public function getFileFields(): array
    {
        return [];
    }

    public function check(): bool
    {
        return true;
    }

    public function getApiName(): string
    {
        return "openapi/customerOrderPro/detail/{$this->orderCode}";
    }

    public function getApiMethod(): string
    {
        return SaasHisConstant::$METHOD_GET;
    }

    public function setOrderCode($orderCode){
        $this->orderCode = $orderCode;
    }

    public function getOrderCode(){
        return $this->orderCode;
    }
}
