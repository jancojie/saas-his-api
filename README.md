# saas-his-api

#### 介绍
岚时HIs接口

#### 安装教程

1.  composer require "jancojie/saas-his-api"

#### 使用说明

```php
use SaasHisApi\Request\OpenApiAuthTokenGetRequest;
use SaasHisApi\Request\OpenApiUserListRequest;
use SaasHisApi\SaasHisClient;


$client = new SaasHisClient();

$tokenRequest = new OpenApiAuthTokenGetRequest;
$tokenRequest->setAppKey('');
$tokenRequest->setAppSecret('');

$res = $client->execute($tokenRequest);
if (isset($data['errorCode']) && $data['errorCode'] == 200) {
    $accessToken = $data['data'];

    $userListRequest = new OpenApiUserListRequest;
    $list = $client->execute($userListRequest,$accessToken);
    var_dump($list);
}

```
#### 参与贡献
Fork 本仓库
新建 Feat_xxx 分支
提交代码
新建 Pull Request